package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"os"
	"strings"
)

// TODO: Create registers that actually make sense
func initializeDb(db *sql.DB) {

	sqlStmt := "create table users (id integer not null primary key, name text, mail text, date_added datetime)"

	_, err := db.Exec(sqlStmt)
	if err != nil {
		log.Fatal(err)
	}

	booksStmt := "create table books (id integer not null primary key, name text, url text, date_added datetime, description text, userid integer,FOREIGN KEY(userid) REFERENCES users(id))"

	_, bkserr := db.Exec(booksStmt)
	if bkserr != nil {
		log.Fatal(bkserr)
	}

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}

	stmt, err := tx.Prepare("insert into books(name, url, date_added) values(?, ?, datetime('now'))")
	if err != nil {
		log.Fatal(err)
	}

	defer stmt.Close()

	for i := 0; i < 100; i++ {
		name := i
		url := fmt.Sprintf("に%03d,BIGGA%03d", i, i)
		_, err = stmt.Exec(name, url)
		if err != nil {
			log.Fatal(err)
		}
	}
	tx.Commit()

}

func removeDB(databaseFile string) {
	var response string
	fmt.Printf("Should we darle tronco a la database ? [Y/N]\n")
	fmt.Scanf("%s", &response)
	if strings.ToLower(response) == "y" {
		os.Remove(databaseFile)
		fmt.Printf("The database has been deleted.\n")
	} else {
		fmt.Printf("Nothing has been done.\n")
	}

}

// GetllAllLinks gets all the links
func GetllAllLinks() *sql.Rows {
	db, err := sql.Open("sqlite3", "./kongoroo.db")
	rows, err := db.Query("select id, url, name, date_added from books")
	if err != nil {
		log.Fatal(err)
	}
	return rows
}

// SaveLink just saves the thing my dude
func SaveLink(book BookMark) (sql.Result, error) {
	db, err := sql.Open("sqlite3", "./kongoroo.db")

	if err != nil {
		return nil, err
	}

	return db.Exec("insert into books(name, url, date_added) values(?, ?, datetime('now'))", book.NAME, book.URL)
}
