package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"net/http"
	"time"
)

//BookMark Main Project structure, nothin' fancy
type BookMark struct {
	ID         int
	NAME       string
	URL        string
	DATE_ADDED time.Time
}

//AddBookMarks Adds bookmarks to the db
func AddBookMarks(w http.ResponseWriter, r *http.Request) {
	var book BookMark
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&book)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	_, e := SaveLink(book)
	if e != nil {
		http.Error(w, fmt.Sprintf("%s \n %s", "It didn't work dummy! te he~", e.Error()), 500)
		return
	}
	GetBookMarks(w, r)
}

//GetBookMarks Gets the bookmarks nicely
func GetBookMarks(w http.ResponseWriter, r *http.Request) {
	rows := GetllAllLinks()
	defer rows.Close()
	var books []BookMark
	for rows.Next() {
		var id int
		var name string
		var url string
		var dateAdded time.Time
		err := rows.Scan(&id, &url, &name, &dateAdded)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		books = append(books, BookMark{ID: id, NAME: name, URL: url, DATE_ADDED: dateAdded})
	}
	err := rows.Err()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	json.NewEncoder(w).Encode(books)
}

func listenAndServe(port string) error {
	log.Printf("Listening on %s", port)
	router := mux.NewRouter()
	router.HandleFunc("/books", AddBookMarks).Methods("POST")
	router.HandleFunc("/books", GetBookMarks).Methods("GET")
	return http.ListenAndServe(port, router)
}

func main() {
	port := ":8000"
	databaseFile := "./kongoroo.db"

	removeDB(databaseFile)
	db, err := sql.Open("sqlite3", databaseFile)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	initializeDb(db)
	log.Fatal(listenAndServe(port))
}
